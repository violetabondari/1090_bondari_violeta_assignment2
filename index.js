
const FIRST_NAME = "Violeta";
const LAST_NAME = "Bondari";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
    let cache = new Object();

    return {
        pageAccessCounter: (page) => 
        {
            page = (page || 'home').toLowerCase();

            cache[page] = typeof cache[page]==='undefined' || cache[page] == null ? 1 : cache[page] + 1; 
        },
        getCache: () => cache
    };
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

